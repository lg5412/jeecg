package org.jeecgframework.web.system.service;

import org.jeecgframework.core.common.service.CommonService;

/**
 * 
 * @author  yanlq
 *
 */
public interface MenuInitService extends CommonService{
	
	public void initMenu();
}
