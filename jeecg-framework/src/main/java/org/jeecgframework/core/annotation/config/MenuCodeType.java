package org.jeecgframework.core.annotation.config;

/**
 * 
 * @author  yanlq
 *
 */
public enum MenuCodeType {
	 TAG, //采用 tag标签 
	 ID,  //采用控件ID方式
	 CSS  //采用Css样式方式
}
