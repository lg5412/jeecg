package org.jeecgframework.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 
 * @author  yanlq
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityTitle {
	  String name();
}
